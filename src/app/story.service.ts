import { Injectable, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from 'src/environments/environment';

export interface Story {
  id: Number;
  user_id: Number;
  name: String;
  description: String;
  created_at: String;
  updated_at: String;
}

@Injectable({
  providedIn: "root"
})
export class StoryService {
  constructor(private http: HttpClient) { }

  getStories(): Observable<Story[]> {
    return this.http.get<Story[]>(environment.apiUrl + "story");
  }

  getStory(storyId): Observable<Story[]> {
    return this.http.get<Story[]>(environment.apiUrl + "story/" + storyId);
  }

  getNodeDecisions(storyId): Observable<Story[]> {
    return this.http.get<Story[]>(environment.apiUrl + "story/" + storyId);
  }
}
