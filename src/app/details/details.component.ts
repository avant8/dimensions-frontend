import { Component, OnInit, Renderer, Renderer2 } from '@angular/core';
import { Story, StoryService } from "../story.service";
import { GameService } from "../game.service";
import { Game } from "../shared/models/game.model";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  game: Game[];
  story: Story[];

  //TODO: Convert to models
  node: Object[];
  game_id: String;
  decisions: Object[];
  monsters: Object[];
  items: Object[];

  constructor(
    private gameService: GameService,
    private route: ActivatedRoute,
    private router: Router,
    private renderer: Renderer2
  ) { }

  ngOnInit() {
    this.game_id = this.route.snapshot.paramMap.get("id");
    this.renderer.addClass(document.body, 'page-light');
    this.getGame();

  }

  newGame(gameId) {
    this.router.navigate(['//game', this.game_id]);
  }

  /**
   * Returns current game state
   */
  getGame() {
    this.gameService.getGame(this.game_id).subscribe((res: any[]) => {
      this.game = res["data"];
      this.story = res["data"]["story"];
      this.node = res["data"]["node"];
      this.decisions = res["data"]["decisions"];
      this.monsters = res["data"]["monsters"];
      this.items = res["data"]["items"];
    });
  }
}