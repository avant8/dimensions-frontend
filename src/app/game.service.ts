import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Story } from "./shared/models/story.model";
import { Game } from "./shared/models/game.model";
import { environment } from '../environments/environment';

@Injectable({
  providedIn: "root"
})
export class GameService {
  constructor(private http: HttpClient) { }

  getGame(gameId): Observable<Object[]> {
    return this.http.get<Object[]>(environment.apiUrl + "game/" + gameId);
  }

  startGame(storyId, userId = 1): Observable<Object[]> {
    return this.http.post<Game[]>(environment.apiUrl + "game/new", {
      story_id: storyId,
      user_id: userId
    });
  }

  makeDecision(gameId, decisionsId, decId): Observable<Object[]> {
    return this.http.post<Game[]>(
      environment.apiUrl + "game/" + gameId + "/destination/" + decisionsId + "/decision/" + decId,
      {}
    );
  }
}
