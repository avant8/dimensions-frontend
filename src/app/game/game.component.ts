import { Component, OnInit, Renderer, Renderer2 } from '@angular/core';
import { Game } from "../shared/models/game.model";
import { Story } from "../shared/models/story.model";
import { GameService } from "../game.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.scss"]
})
export class GameComponent implements OnInit {
  game: Game[];
  story: Story[];

  //TODO: Convert to models
  node: Object[];
  game_id: String;
  decisions: Object[];
  monsters: Object[];
  items: Object[];

  constructor(
    private gameService: GameService,
    private route: ActivatedRoute,
    private router: Router,
    private renderer: Renderer2
  ) { }

  ngOnInit() {
    this.game_id = this.route.snapshot.paramMap.get("id");
    this.renderer.removeClass(document.body, 'page-light');
    this.getGame();
  }

  /**
   * Returns current game state
   */
  getGame() {
    this.gameService.getGame(this.game_id).subscribe((res: any[]) => {
      this.game = res["data"];
      this.story = res["data"]["story"];
      this.node = res["data"]["node"];
      this.decisions = res["data"]["decisions"];
      this.monsters = res["data"]["monsters"];
      this.items = res["data"]["items"];
    });
  }

  /**
   * Call API with new decision
   *
   * @param {*} decisionId
   * @param {*} gameId
   * @memberof GameComponent
   */
  makeDecision(decisionId, gameId, decId) {
    this.gameService.makeDecision(gameId, decisionId, decId).subscribe(res => {
      //TODO: !Add animation for new decision
      this.router.navigate([
        "/game/" + res["data"]["id"]
      ]);
      this.game = res["data"];
      this.story = res["data"]["story"];
      this.node = res["data"]["node"];
      this.decisions = res["data"]["decisions"];
      this.monsters = res["data"]["monsters"];
      this.items = res["data"]["items"];
    });
  }
}
